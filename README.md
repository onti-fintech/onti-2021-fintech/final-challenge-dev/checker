n2n-oracle
===

1. Setup `.env`:
```bash=
cp .env.example .env
```

2. Run:
```bash=
docker-compose up --build --abort-on-container-exit
```