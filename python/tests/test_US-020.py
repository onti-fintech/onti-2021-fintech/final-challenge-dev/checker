import pytest
import random
import time
import os
import re
from helpers import deploy_contracts, start_oracle, setLiquidity, check_transactions, exchange_and_check
from blockchain_tools import send_transaction_and_check, check_events
from docker_tools import  start_container, stop_container

from . import US_018
@pytest.mark.it('AC-020-01: Non-automatic sending of Tokens')
@pytest.mark.dependency(depends= US_018, name='AC_020_01', scope='session')
def test_AC_20_01(docker, log, web3_left, web3_right, accounts, \
                   default_deployment_environment):
    log.info('Testing AC_020_01.')

    account1 = web3_right.eth.account.from_key(web3_left.keccak(os.urandom(32)))
    account2 = web3_right.eth.account.from_key(web3_left.keccak(os.urandom(32)))
    account3 = web3_right.eth.account.from_key(web3_left.keccak(os.urandom(32)))



    VALIDATORS = [account1, account2, account3]
    VALIDATORS_ADDRESSES = list(map(lambda v: v.address, VALIDATORS))

    send_transaction_and_check(web3_right, accounts[0], value=web3_right.toWei('100000', 'ether'), to=account1.address)
    send_transaction_and_check(web3_right, accounts[0], value=web3_right.toWei('100000', 'ether'), to=account2.address)
    send_transaction_and_check(web3_right, accounts[0], value=web3_right.toWei('100000', 'ether'), to=account3.address)

    default_deployment_environment['VALIDATORS'] = ' '.join(map(str, VALIDATORS_ADDRESSES))
    default_deployment_environment['THRESHOLD'] = 2

    tmp = deploy_contracts(docker, log, web3_left, web3_right, default_deployment_environment)

    bridge_left = tmp['left']['bridge']
    bridge_right = tmp['right']['bridge']

    environment = {
        'PRIVKEY': accounts[7]._private_key.hex(),
        'LEFT_RPCURL': 'http://geth-left:8545',
        'RIGHT_RPCURL': 'http://geth-right:8545',
        'LEFT_GASPRICE': '20000000000',
        'RIGHT_GASPRICE': '20000000000',
        'ORACLE_DATA': f'/mnt/solution-data/{accounts[7].address}',

        'LEFT_ADDRESS': bridge_left.address,
        'RIGHT_ADDRESS': bridge_right.address,
        'LEFT_START_BLOCK': bridge_left.deployment_block,
        'RIGHT_START_BLOCK': bridge_left.deployment_block
    }

    send_transaction_and_check(web3_left, accounts[0],
                               function=bridge_left.functions.enableRobustMode(),
                               value=0)
    send_transaction_and_check(web3_right, accounts[0],
                               function=bridge_right.functions.enableRobustMode(),
                               value=0)

    ETH_1000 =  web3_right.toWei('1000', 'ether')
    setLiquidity(bridge_left, bridge_right, accounts[0], ETH_1000)

    for validator in VALIDATORS:
        start_oracle(log, docker, bridge_left, bridge_right, validator)

    exchange_and_check(bridge_left, bridge_right, accounts[5], \
                       ETH_1000, VALIDATORS_ADDRESSES, 2)

    start_balance = web3_left.eth.get_balance(accounts[6].address)
    txn_receipt = send_transaction_and_check(web3_right, accounts[6], value=web3_right.toWei(10, 'ether'), to=bridge_right.address)
    start_block = web3_left.eth.block_number
    time.sleep(15)
    finish_block = web3_left.eth.block_number
    finish_balance = web3_left.eth.get_balance(accounts[6].address)
    assert finish_balance == start_balance, \
        f'Bridge sends Unauthorised transaction'

    assert start_block == finish_block, \
        f'Transaction count mismatch: Validators sent txn'

    path = '/tools/applyCommits.py'
    tx_hash = txn_receipt['transactionHash']
    log.info('TXN_HASH ' + tx_hash.hex())

    container = start_container(docker, 'n2n-oracle', log, \
                                command=[path, tx_hash.hex()], environment=environment)
    container.wait(timeout=15)
    # Stop the container.
    container.stop()

    # Update state.
    container.reload()

    log.info(f'Stopping `{container.name}` container.')
    out = container.logs(stderr=False).decode()
    if out:
        match = re.search(r'(0x\w{64}) executed', out)
        assert match, \
            f'CLI produced wrong result'
        txHash = match.group(1)
    else:
        raise Exception(f'Empty CLI output')

    try:
        txn = web3_left.eth.getTransactionReceipt(txHash)
    except Exception:
        raise Exception(f'Transaction {tx_hash} not found')
    finish_balance = web3_left.eth.get_balance(accounts[6].address)
    assert finish_balance == start_balance + web3_left.toWei(10, 'ether'), \
        f'balance of recipient didnt changes'