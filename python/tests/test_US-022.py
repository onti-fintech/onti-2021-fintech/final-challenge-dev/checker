import pytest
import random
import time
from helpers import deploy_contracts, start_oracle, setLiquidity, check_transactions, exchange_and_check, random_transaction_id
from blockchain_tools import send_transaction_and_check, check_events

from . import US_008, US_009, US_010, US_011


@pytest.mark.it('AC-022-01: Min limits')
@pytest.mark.dependency(depends=US_008 + US_009 + US_010 + US_011, name='AC_022_01', scope='session')
def test_AC_022_01(docker, log, web3_left, web3_right, accounts, \
                   default_deployment_environment):
    log.info('Testing AC_022_01.')
    VALIDATORS = [accounts[1], accounts[2], accounts[3]]
    VALIDATORS_ADDRESSES = list(map(lambda v: v.address, VALIDATORS))

    default_deployment_environment['VALIDATORS'] = ' '.join(map(str, VALIDATORS_ADDRESSES))
    default_deployment_environment['THRESHOLD'] = 1

    tmp = deploy_contracts(docker, log, web3_left, web3_right, default_deployment_environment)

    bridge_left = tmp['left']['bridge']
    bridge_right = tmp['right']['bridge']

    ETH_2000 =  web3_right.toWei('2000', 'ether')
    setLiquidity(bridge_left, bridge_right, accounts[0], ETH_2000)
    
    send_transaction_and_check(web3_left, accounts[5], 
                               value=web3_left.toWei(50, 'ether'),
                               to=bridge_left.address)

    send_transaction_and_check(web3_left, accounts[0],
                               function=bridge_left.functions.setMinPerTx(49999999999999999999),
                               value=0)

    send_transaction_and_check(web3_left, accounts[5], revert=True,
                               value=web3_left.toWei(40, 'ether'),
                               to=bridge_left.address)

    send_transaction_and_check(web3_left, accounts[5],
                               value=web3_left.toWei(50, 'ether'),
                               to=bridge_left.address)
        
    receiver, value = accounts[5].address, web3_left.toWei(1000, 'ether')
    txReceipt = send_transaction_and_check(web3_left, accounts[5],
                               value=value,
                               to=bridge_left.address)
    txHash = txReceipt['transactionHash']
    
    balance_before = web3_right.eth.get_balance(receiver)
    send_transaction_and_check(web3_right, VALIDATORS[0],
                            function=bridge_right.functions.commit(receiver, value, txHash),
                            value=0,
                            )

    assert web3_right.eth.get_balance(receiver) == balance_before + value, \
        "Balance dismatch"

    send_transaction_and_check(web3_left, accounts[5],
                               value=web3_left.toWei(50, 'ether'),
                               to=bridge_left.address)

    send_transaction_and_check(web3_left, accounts[0],
                               function=bridge_left.functions.setMinPerTx(49999999999999999999),
                               value=0)

    send_transaction_and_check(web3_left, accounts[5], revert=True,
                               value=web3_left.toWei(40, 'ether'),
                               to=bridge_left.address)

    send_transaction_and_check(web3_left, accounts[5],
                               value=web3_left.toWei(50, 'ether'),
                               to=bridge_left.address)


@pytest.mark.it('AC-022-02: Max limits')
@pytest.mark.dependency(depends=[], name='AC_022_02', scope='session')
def test_AC_022_02(docker, log, web3_left, web3_right, accounts, \
                   default_deployment_environment):
    log.info('Testing AC_022_02.')
    VALIDATORS = [accounts[1], accounts[2], accounts[3]]
    VALIDATORS_ADDRESSES = list(map(lambda v: v.address, VALIDATORS))

    default_deployment_environment['VALIDATORS'] = ' '.join(map(str, VALIDATORS_ADDRESSES))
    default_deployment_environment['THRESHOLD'] = 1

    tmp = deploy_contracts(docker, log, web3_left, web3_right, default_deployment_environment)

    bridge_left = tmp['left']['bridge']
    bridge_right = tmp['right']['bridge']
    
    ETH_2000 =  web3_right.toWei('2000', 'ether')
    ETH_100 =  web3_right.toWei('100', 'ether')
    ETH_99 =  web3_right.toWei('99', 'ether')
    
    setLiquidity(bridge_left, bridge_right, accounts[0], ETH_2000)


    receiver, value = accounts[5].address, web3_left.toWei(1000, 'ether')
    txReceipt = send_transaction_and_check(web3_left, accounts[5],
                               value=value,
                               to=bridge_left.address)
    txHash = txReceipt['transactionHash']
    
    balance_before = web3_right.eth.get_balance(receiver)
    send_transaction_and_check(web3_right, VALIDATORS[0],
                            function=bridge_right.functions.commit(receiver, value, random_transaction_id()),
                            value=0,
                            )
    assert web3_right.eth.get_balance(receiver) == balance_before + value, \
        "Balance dismatch"

    
    value = ETH_100
    receipt = send_transaction_and_check(web3_left, VALIDATORS[0],
                            function=bridge_left.functions.commit(receiver, value, random_transaction_id()),
                            value=0,
                            )

    send_transaction_and_check(web3_left, accounts[5], 
                               value=ETH_100,
                               to=bridge_left.address)


    send_transaction_and_check(web3_left, accounts[0],
                               function=bridge_left.functions.setMaxPerTx(ETH_100 - 1),
                               value=0)
    
    txHash = random_transaction_id()
    send_transaction_and_check(web3_left, VALIDATORS[0], revert=True,
                            function=bridge_left.functions.commit(receiver, ETH_100, random_transaction_id()),
                            value=0,
                            )

    balance_before = web3_left.eth.get_balance(receiver)
    send_transaction_and_check(web3_left, VALIDATORS[0],
                            function=bridge_left.functions.commit(receiver, ETH_99, random_transaction_id()),
                            value=0,
                            )
    assert web3_left.eth.get_balance(receiver) == balance_before + ETH_99


    
    send_transaction_and_check(web3_left, accounts[5], revert=True,
                               value=ETH_100,
                               to=bridge_left.address)

    send_transaction_and_check(web3_left, accounts[5],
                               value=ETH_99,
                               to=bridge_left.address)


    # === RIGHT ===
    
    # normal
    balance_before = web3_right.eth.get_balance(receiver)
    send_transaction_and_check(web3_right, VALIDATORS[0],
                            function=bridge_right.functions.commit(receiver, ETH_100, random_transaction_id()),
                            value=0,
                            )
    assert web3_right.eth.get_balance(receiver) == balance_before + ETH_100

    send_transaction_and_check(web3_right, accounts[5],
                               value=ETH_100,
                               to=bridge_right.address)

    # with limit
    send_transaction_and_check(web3_right, accounts[0],
                               function=bridge_right.functions.setMaxPerTx(ETH_100 - 1),
                               value=0)

    send_transaction_and_check(web3_right, VALIDATORS[0], revert=True,
                            function=bridge_right.functions.commit(receiver, ETH_100, random_transaction_id()),
                            value=0,
                            )

    balance_before = web3_right.eth.get_balance(receiver)
    send_transaction_and_check(web3_right, VALIDATORS[0],
                            function=bridge_right.functions.commit(receiver, ETH_99, random_transaction_id()),
                            value=0,
                            )
    assert web3_right.eth.get_balance(receiver) == balance_before + ETH_99

    send_transaction_and_check(web3_right, accounts[5], revert=True,
                               value=ETH_100,
                               to=bridge_right.address)

    send_transaction_and_check(web3_right, accounts[5],
                               value=ETH_99,
                               to=bridge_right.address)
