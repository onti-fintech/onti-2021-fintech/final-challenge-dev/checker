import pytest

from helpers import deploy_contracts, start_oracle, stop_oracle, \
    setLiquidity, exchange_and_check, random_transaction_id

from web3.eth import Account

from eth_account.messages import encode_defunct

from blockchain_tools import send_transaction_and_check, check_events

from . import US_002


def sign_message(account:Account, message:bytes) -> tuple[int, int, int]:    
    signed = account.sign_message(encode_defunct(message))
    
    return signed.r, signed.s, signed.v


@pytest.mark.it('AC-023-01: Trusted mode.')
@pytest.mark.dependency(depends = US_002, name = 'AC_023_01', scope = 'session')
def test_AC_023_01(docker, log, web3_left, web3_right, accounts, \
    default_deployment_environment):

    log.info('Testing AC_023_01.')

    # One validator.
    default_deployment_environment['VALIDATORS'] = accounts[1].address
    default_deployment_environment['THRESHOLD'] = 1

    tmp = deploy_contracts(docker, log, web3_left, web3_right, default_deployment_environment)

    bridge_left = tmp['left']['bridge']
    bridge_right = tmp['right']['bridge']  
    
    # Update liquidity.
    setLiquidity(bridge_left, bridge_right, accounts[0], \
        web3_left.toWei('2000', 'ether'))
    
    # Before send
    balanсe_left_contract \
        = web3_left.eth.get_balance(bridge_left.address)

    # 1. Send to the left.
    send_transaction_and_check(web3_left, accounts[5], \
        to = bridge_left.address,
        value = web3_left.toWei('50', 'ether'))

    # LEft contract.
    assert web3_left.eth.get_balance(bridge_left.address) == balanсe_left_contract + web3_left.toWei('50', 'ether'), \
        f'1. Bad left contract balance: {web3_left.eth.get_balance(bridge_left.address)} != {balanсe_left_contract + web3_left.toWei("50", "ether")}.' 


    # 2. stopOperations() on LEFT
    send_transaction_and_check(web3_left, accounts[0], \
        function=bridge_left.functions.stopOperations())


    # 3. Cannot exchange.web3_left
    send_transaction_and_check(web3_left, accounts[5], revert=True, \
                               to=bridge_left.address,
                               value=web3_left.toWei('50', 'ether'))

    # 4. Validator.
    send_transaction_and_check(web3_left, accounts[1], \
            function = bridge_left.functions.commit(\
                accounts[5].address, web3_right.toWei('50','ether'), random_transaction_id()))



    send_transaction_and_check(web3_left, accounts[0], \
                               function=bridge_left.functions.startOperations())

    send_transaction_and_check(web3_left, accounts[5], \
                               to=bridge_left.address,
                               value=web3_left.toWei('50', 'ether'))

    send_transaction_and_check(web3_left, accounts[1], \
                               function=bridge_left.functions.commit( \
                                   accounts[5].address, web3_left.toWei('50', 'ether'), random_transaction_id()))

    ### RIGT BRIDGE TESTING====================================

    send_transaction_and_check(web3_right, accounts[1], \
            function = bridge_right.functions.commit(\
                accounts[5].address, web3_right.toWei('50','ether'), random_transaction_id()))

    balanсe_right_contract \
        = web3_right.eth.get_balance(bridge_right.address)

    # 8. Send to the left.
    send_transaction_and_check(web3_right, accounts[5], \
                               to=bridge_right.address,
                               value=web3_right.toWei('5', 'ether'))

    # Right contract.
    assert web3_right.eth.get_balance(bridge_right.address) == balanсe_right_contract + web3_right.toWei('5', 'ether'), \
        f'1. Bad left contract balance: {web3_right.eth.get_balance(bridge_right.address)} != {balanсe_right_contract + web3_right.toWei("50", "ether")}.'


    # 2. stopOperations() on Right
    send_transaction_and_check(web3_right, accounts[0], \
                               function=bridge_right.functions.stopOperations())

    # 3. Cannot exchange.
    send_transaction_and_check(web3_right, accounts[5], revert=True, \
                               to=bridge_right.address,
                               value=web3_right.toWei('5', 'ether'))

    # 4. Validator.
    send_transaction_and_check(web3_right, accounts[1], \
                               function=bridge_right.functions.commit( \
                                   accounts[5].address, web3_right.toWei('50', 'ether'), random_transaction_id()))

    send_transaction_and_check(web3_right, accounts[0], \
                               function=bridge_right.functions.startOperations())

    send_transaction_and_check(web3_right, accounts[5], \
                               to=bridge_right.address,
                               value=web3_right.toWei('5', 'ether'))




