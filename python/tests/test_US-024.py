import pytest
from blockchain_tools import send_transaction_and_check, get_balance, send_transaction
from helpers import deploy_contracts, start_oracle, check_transactions, setLiquidity, exchange_and_check
import time
from helpers import findEventInTx
from . import US_012, US_013, US_018


@pytest.mark.it('AC-024-01: Send confirmations in default mode. ')
@pytest.mark.dependency(depends=US_012 + US_013 + US_018, name='AC_024_01', scope='session')
def test_AC_024_01(docker, log, web3_left, web3_right, accounts, default_deployment_environment):
    log.info('Testing AC_024_01.')
    default_deployment_environment['VALIDATORS'] = accounts[1].address
    default_deployment_environment['THRESHOLD'] = 1
    default_deployment_environment['REQUIRED_CONFIRMATIONS'] = 12

    result = deploy_contracts(docker, log, web3_left, web3_right, default_deployment_environment)

    bridge_right = result['right']['bridge']
    bridge_left = result['left']['bridge']

    setLiquidity(bridge_left, bridge_right, accounts[0], web3_left.toWei('2000', 'ether'))

    start_oracle(log, docker, bridge_left, bridge_right, accounts[1], required_confirmations=12)

    block_start = web3_right.eth.block_number + 1

    send_transaction(web3_left, accounts[5], to=bridge_left.address, value=web3_left.toWei('1000', 'ether'))

    for i in range(default_deployment_environment['REQUIRED_CONFIRMATIONS'] - 1):
        send_transaction(web3_left, accounts[10], to=accounts[11].address, value=1)
    check_transactions(web3_right, {accounts[1].address}, block_start, 0)
    send_transaction(web3_left, accounts[10], to=accounts[11].address, value=1)
    check_transactions(web3_right, {accounts[1].address}, block_start, 1)

    block_start = web3_left.eth.block_number + 1

    send_transaction(web3_right, accounts[5], to=bridge_right.address, value=web3_left.toWei('50', 'ether'))

    for i in range(default_deployment_environment['REQUIRED_CONFIRMATIONS'] - 1):
        send_transaction(web3_right, accounts[10], to=accounts[11].address, value=1)
    time.sleep(16)
    check_transactions(web3_left, {accounts[1].address}, block_start, 0)
    send_transaction(web3_right, accounts[10], to=accounts[11].address, value=1)
    check_transactions(web3_left, {accounts[1].address}, block_start, 1)


@pytest.mark.it('AC-024-02: Send confirmations in robust mode. ')
@pytest.mark.dependency(depends=['AC_024_01'], name='AC_024_02', scope='session')
def test_AC_024_02(docker, log, web3_left, web3_right, accounts, default_deployment_environment):
    log.info('Testing AC_024_02.')
    default_deployment_environment['VALIDATORS'] = accounts[1].address
    default_deployment_environment['THRESHOLD'] = 1
    default_deployment_environment['REQUIRED_CONFIRMATIONS'] = 12

    result = deploy_contracts(docker, log, web3_left, web3_right, default_deployment_environment)

    bridge_right = result['right']['bridge']
    bridge_left = result['left']['bridge']

    setLiquidity(bridge_left, bridge_right, accounts[0], web3_left.toWei('2000', 'ether'))
    send_transaction_and_check(web3_left, accounts[0],
                               function=bridge_left.functions.enableRobustMode())
    send_transaction_and_check(web3_right, accounts[0],
                               function=bridge_right.functions.enableRobustMode())
    start_oracle(log, docker, bridge_left, bridge_right, accounts[1], required_confirmations=12)

    block_start = web3_right.eth.block_number + 1

    send_transaction(web3_left, accounts[5], to=bridge_left.address, value=web3_left.toWei('1000', 'ether'))

    for i in range(default_deployment_environment['REQUIRED_CONFIRMATIONS'] - 1):
        send_transaction(web3_left, accounts[10], to=accounts[11].address, value=1)
    check_transactions(web3_right, {accounts[1].address}, block_start, 0)

    send_transaction(web3_left, accounts[10], to=accounts[11].address, value=1)
    check_transactions(web3_right, {accounts[1].address}, block_start, 1)

    block_start = web3_left.eth.block_number + 1

    send_transaction(web3_right, accounts[5], to=bridge_right.address, value=web3_left.toWei('50', 'ether'))

    for i in range(default_deployment_environment['REQUIRED_CONFIRMATIONS'] - 1):
        send_transaction(web3_right, accounts[10], to=accounts[11].address, value=1)
    time.sleep(16)

    check_transactions(web3_left, {accounts[1].address}, block_start, 0)
    check_transactions(web3_right, {accounts[1].address}, web3_right.eth.block_number + 1, 0)
    block = web3_right.eth.block_number + 2
    send_transaction(web3_right, accounts[10], to=accounts[11].address, value=1)

    txs = check_transactions(web3_right, {accounts[1].address}, block, 1)
    log.info("СОСЫСКА")
    f = findEventInTx(bridge_right, 'commitsCollected', txs[0])
    assert f
    # log.info(f)
    check_transactions(web3_left, {accounts[1].address}, block_start, 1)
