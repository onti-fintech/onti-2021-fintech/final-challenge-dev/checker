import os
import json

from web3 import Web3
from web3.eth import Account, Contract
from web3.logs import DISCARD
from web3._utils.filters import LogFilter


# Sends transaction. If `function` in kwargs, sends contract call. 
# If `receipt` is `True` - returns receipt, else - transaction hash.
def send_transaction(web3: Web3, account: Account, receipt=True, revert=False, \
                     **kwargs) -> dict:
    tx = {
        'from':     account.address,
        'nonce':    web3.eth.getTransactionCount(account.address),

        'gasPrice': 20000000000
    }

    # Custom parameters.
    tx.update(kwargs)

    try:
        # Contract call.
        if 'function' in tx:
            # Geth.
            gas_price = tx.pop('gasPrice')

            tx = tx.pop('function').buildTransaction(tx)

            tx['gasPrice'] = gas_price
        
        else:
            tx['gas'] = web3.eth.estimateGas(tx)

    except Exception as exc:
        # Should revert.
        if revert:
            return None

        raise exc

    # If not reverted.
    assert not revert, \
        f'Transaction should be reverted:\n{json.dumps(tx, indent=4, sort_keys=True)}.'

    tx_hash = web3.eth.sendRawTransaction(account.sign_transaction(tx).rawTransaction)

    if receipt:
        return web3.eth.waitForTransactionReceipt(tx_hash)

    return tx_hash


# Sends transaction and checks that status == `status`,
# If `receipt` is `True` - returns receipt, else - transaction hash.
def send_transaction_and_check(web3: Web3, account: Account, status=1, receipt=True, \
                               revert=False, **kwargs) -> dict:
    try:
        tx_receipt = send_transaction(web3, account, receipt, revert, **kwargs)

    except Exception as exc:
        raise Exception(f'Cannot execute transaction: {exc}')

    if revert:
        assert tx_receipt is None, \
            'Transaction should be reverted: {tx_reseipt}.'

        return None

    if receipt:
        # Checks only of we wait receipt.
        assert int(tx_receipt.status) == status, \
            f'Transaction status mismatch: {tx_receipt.status}'

    return tx_receipt


# Checks that number of events is equal to len(args)
# and that i-th arg equal to i-th event arg. 
def check_events(events: [LogFilter, list[dict]], *args: list[dict]) -> list[dict]:
    if isinstance(events, LogFilter):
        events = events.get_new_entries()

    assert len(events) == len(args), f'Too few or too much events. {len(events)} != {len(args)}.'

    for (event, kwargs) in zip(events, args):
        for (key, value) in kwargs.items():
            assert event.args.get(key) == value, \
                f'Event argument mismatch: Key = {key}, Event: {event.args.get(key)}, Value: {value}'

    return events


def get_balance(web3: Web3, address: str) -> int:
    return web3.eth.get_balance(address)
