import json
import os
import re
import time
from logging import Logger
from shutil import rmtree
from web3.logs import DISCARD

import pytest
from docker import DockerClient
from web3 import Web3
from web3.eth import Account, Contract

from blockchain_tools import send_transaction_and_check
from docker_tools import (compose_down, compose_up, run_in_solution_dir,
                          start_container, stop_container)

# ABI.
with open('./abi/bridge_left.json') as file:
    bridge_left_abi = json.load(file)

with open('./abi/bridge_right.json') as file:
    bridge_right_abi = json.load(file)

with open('./abi/validators_set.json') as file:
    validators_set_abi = json.load(file)


def deploy_contracts(docker: DockerClient, log: Logger, web3_left: Web3, web3_right: Web3, \
                     environment: dict) -> dict:
    # $ docker run -ti --rm --env-file .env n2n-oracle /deployment/run.sh
    container = start_container(docker, 'n2n-oracle', log, \
                                command=['/deployment/run.sh'], environment=environment)

    # Wait.
    container.wait(timeout=40)

    # Update state.
    container.reload()

    # Get exit code.
    exit_code = container.attrs['State']['ExitCode']

    # Save output.
    logs = container.logs(stderr=False).decode().strip().split('\n')

    # Remove.
    stop_container(docker, container, log)

    # Should be ok.
    assert exit_code == 0, \
        f'Bad exit code: {exit_code}.'

    # Expect six lines.
    assert len(logs) == 6, \
        f'Lines count mismatch: {len(logs)} != 6.'

    # Patters.
    patters = [
        r'#1\s\[LEFT\]\sValidators\sSet\sdeployed\sat\s0x[A-Fa-f0-9]{40}$',
        r'#2\s\[LEFT\]\sBridge\sdeployed\sat\s0x[A-Fa-f0-9]{40}$',
        r'#3\s\[RIGHT\]\sValidators\sSet\sdeployed\sat\s0x[A-Fa-f0-9]{40}$',
        r'#4\s\[RIGHT\]\sBridge\sdeployed\sat\s0x[A-Fa-f0-9]{40}$',
        r'#5\s\[LEFT\]\sBridge\sdeployed\sat\sblock\s[0-9]+$',
        r'#6\s\[RIGHT\]\sBridge\sdeployed\sat\sblock\s[0-9]+$'
    ]

    # Check lines.
    for i in range(6):
        assert re.fullmatch(patters[i], logs[i]), \
            f'Line {i + 1} mismatch:\n`{logs[i]}`.'

    validators_set_left = web3_left.eth.contract( \
        abi=validators_set_abi, address=re.findall(r'\s0x[A-Fa-f0-9]{40}$', logs[0])[0].strip())
    validators_set_right = web3_right.eth.contract( \
        abi=validators_set_abi, address=re.findall(r'\s0x[A-Fa-f0-9]{40}$', logs[2])[0].strip())

    bridge_left = web3_left.eth.contract( \
        abi=bridge_left_abi, address=re.findall(r'\s0x[A-Fa-f0-9]{40}$', logs[1])[0].strip())
    bridge_right = web3_right.eth.contract( \
        abi=bridge_right_abi, address=re.findall(r'\s0x[A-Fa-f0-9]{40}$', logs[3])[0].strip())

    bridge_left.deployment_block = int(re.findall(r'\s[0-9]+$', logs[4])[0])
    bridge_right.deployment_block = int(re.findall(r'\s[0-9]+$', logs[5])[0])

    return {
        'left': {
            'validators_set': validators_set_left,
            'bridge': bridge_left
        },

        'right': {
            'validators_set': validators_set_right,
            'bridge': bridge_right
        }
    }


def random_transaction_id() -> str:
    return '0x' + os.urandom(32).hex()


def start_oracle(log: Logger, docker: DockerClient, bridge_left: Contract, \
                 bridge_right: Contract, owner: Account, required_confirmations=0):
    # To be removed after test.
    pytest.oracles.append(owner.address)

    environment = {
        'PRIVKEY': owner._private_key.hex(),
        'LEFT_RPCURL': 'http://geth-left:8545',
        'RIGHT_RPCURL': 'http://geth-right:8545',
        'LEFT_GASPRICE': '20000000000',
        'RIGHT_GASPRICE': '20000000000',
        'ORACLE_DATA': f'/mnt/solution-data/{owner.address}',

        'LEFT_ADDRESS': bridge_left.address,
        'RIGHT_ADDRESS': bridge_right.address,
        'LEFT_START_BLOCK': bridge_left.deployment_block,
        'RIGHT_START_BLOCK': bridge_right.deployment_block,
        'REQUIRED_CONFIRMATIONS': required_confirmations,
    }

    assert compose_up(log, environment, owner.address) == 0, \
        f'Docker-compose up returns non-zero status.'

    container_found = False
    for container in docker.containers.list():
        # docker ps | grep n2n-oracle
        container_found = container_found or ('n2n-oracle' in container.image.tags[0])

    assert container_found, \
        'No one of the container based on `n2n-oracle` image.'

    # Default compose network.
    network = docker.networks.get(f'{owner.address.lower()}_default')

    # Connect nodes to the oracle.
    network.connect(docker.containers.get('geth-left'), aliases=['geth-left'])
    network.connect(docker.containers.get('geth-right'), aliases=['geth-right'])


def stop_oracle(log: Logger, docker: DockerClient, address: Account, remove_db=False):
    try:
        # Default compose network.
        network = docker.networks.get(f'{address.lower()}_default')

        # Disconnect nodes from the oracle.
        network.disconnect(docker.containers.get('geth-left'))
        network.disconnect(docker.containers.get('geth-right'))

    except Exception as exc:
        log.error(f'Error while disconnecting nodes: {exc}')

    exit_code = compose_down(log, address)

    if remove_db:
        # Only if this function is safe.
        assert rmtree.avoids_symlink_attacks

        # Remove db.
        rmtree(f'./solution-data/{address}', ignore_errors=True)

    return exit_code


def setLiquidity(bridge_left, bridge_right, owner_account, valueInWei):
    """
    Set the liquidity on `bridge_left` and `bridge_bridge` 
    to `value` (in ether) using `owner_account`
    """
    web3_left = bridge_left.web3
    web3_right = bridge_right.web3

    send_transaction_and_check(web3_right, owner_account,
                               function=bridge_right.functions.addLiquidity(),
                               value=valueInWei)

    assert bridge_right.functions.getLiquidityLimit().call() == valueInWei

    send_transaction_and_check(web3_left, owner_account,
                               function=bridge_left.functions.updateLiquidityLimit(valueInWei),
                               value=0)

    assert bridge_left.functions.getLiquidityLimit().call() == valueInWei


# Get transactions from blocks and checks them.
def check_transactions(web3: Web3, senders: set[str], block_start: int, count: int, \
                       timeout=15) -> list[dict]:
    # No txs.
    if count == 0:
        block_dst = block_start
        assert block_dst, \
            'To be able check zero transactions provide block_dst.'

        # Wait txs.
        time.sleep(timeout)
        # Dst block.
        block_dst_ = web3.eth.block_number + 1
        # No tx.
        assert block_dst == block_dst_, \
            f'Non zero trnsaction count: {block_dst_ - block_dst}.'

        return

    # How many tx present.
    processed = 0

    while processed < count:
        # Timeout.
        assert timeout > 0, \
            f'Timeout, not all transactions present: {processed} out of {count}.'

        # If not such block.
        if (block_start > web3.eth.block_number):
            # Wait 100ms.
            time.sleep(0.1)

            # Timeout only on wainting.
            timeout -= 0.1

            # Try again.
            continue
        txs = []
        # Process transactions.
        for tx_hash in web3.eth.get_block(block_start).transactions:
            # Receipt.
            tx_receipt = web3.eth.getTransactionReceipt(tx_hash.hex())

            # Check sender.
            assert tx_receipt['from'] in senders, \
                f'Wrong sender: should be one of {senders}, not {tx_receipt["from"]}.'

            # Count.
            processed += 1
            txs.append(tx_receipt)

        # Next block.
        block_start += 1

    # If more then need.
    assert processed == count, \
        f'Transaction count mismatch: should be {count}, not {processed}.'
    return txs


def exchange_and_check(src: Contract, dst: Contract, account: Account, value: int, \
                       validators: set, threshold: int, before_check_callback=None, \
                       state=None, timeout=15) -> None:
    # If state is not None - check only.
    if state is None:
        # Expect tx in this block.
        block_dst = dst.web3.eth.block_number + 1

        # Balance of source contrat.
        balance_src = src.web3.eth.get_balance(src.address)
        # Balance of account in destination.
        balance_dst = dst.web3.eth.get_balance(account.address)
        # Balance of destination contract.
        contract_dst = dst.web3.eth.get_balance(dst.address)

        # Send to the source contract.
        send_transaction_and_check(src.web3, account, \
                                   to=src.address, value=value)

        # Balance of source contract updted.
        balance_src_ = src.web3.eth.get_balance(src.address)

        # Source balance.
        assert balance_src + value == balance_src_, \
            f'Source balance mismatch: {balance_src + value} != {balance_src_}.'

    else:
        block_dst = state['block_dst']
        balance_dst = state['balance_dst']
        contract_dst = state['contract_dst']

    # Callback.
    if before_check_callback:
        before_check_callback()

    # No txs.
    if threshold == 0:
        time.sleep(timeout)
        # Dst block.
        block_dst_ = dst.web3.eth.block_number + 1
        # No tx.
        assert block_dst == block_dst_, \
            f'Non zero trnsaction count: {block_dst_ - block_dst}.'

        return

    # Validators transactions.
    check_transactions(dst.web3, validators, block_dst, threshold, timeout)

    # Balance of account in destination.
    balance_dst_ = dst.web3.eth.get_balance(account.address)

    # Destination balance.
    assert balance_dst + value == balance_dst_, \
        f'Destination balance mismatch: {balance_dst + value} != {balance_dst_}.'

    # Balance of destination contract.
    contract_dst_ = dst.web3.eth.get_balance(dst.address)

    # Destination contract balance.
    assert contract_dst - value <= contract_dst_, \
        f'Destination contract balance mismatch: {balance_dst - value} > {balance_dst_}.'


def findEventInTx(contract, eventName, txReceipt):
    events = contract.events[eventName]().processReceipt(
        txReceipt, errors=DISCARD)
    if events:
        return events[0]
