import os
import json

from logging import Logger

from subprocess import run as run_process

from docker        import DockerClient
from docker.errors import ImageNotFound


# Start the container.
def start_container(docker:DockerClient, image:[str, 'Image'], \
    log:Logger, **kwargs) -> 'Container':

    default_kwargs = {
        'detach': True,

        # To be able to access other containers.
        'network': 'fintech-network',
    }
    
    default_kwargs.update(kwargs)

    if isinstance(image, str):
        try:
            # Get by name.
            image = docker.images.get(image)
        except ImageNotFound:
            # Try pull.
            image = docker.images.pull(image)
    
    log.debug(f'Starting container with {image.tags} image.')
    for line in json.dumps(default_kwargs, indent = 4, sort_keys = True).split('\n'):
            log.debug(line)

    container = docker.containers.run(image, **default_kwargs)

    log.info(f'Container `{container.name}` started.')

    return container

# Stop and remove container.
def stop_container(docker:DockerClient, container:[str, 'Container'], \
    log:Logger) -> None:

    if isinstance(container, str):
        # Get by name.
        container = docker.containers.get(container)
    
    # Update state.
    container.reload()

    log.info(f'Stopping `{container.name}` container.')
    
    # Check logs.
    out = container.logs(stderr = False).decode()
    err = container.logs(stdout = False).decode()

    if out:
        log.info('Contrainer STDOUT.')
        for line in out.split('\n'):
            log.info(line)

    if err:
        log.error('Contrainer STDERR.')
        for line in err.split('\n'):
            log.error(line)

    # Stop the container.
    container.stop()

    # Remove.
    container.remove()

def run_in_solution_dir(log:Logger, command:list[str]) -> int:
    process = run_process(command, \
        capture_output = True, env = {'PATH': os.environ['PATH']}, cwd = './solution')

    command = ' '.join(command)

    if process.stdout:
        log.info(f'Run `{command}`, stdout.')
        for line in process.stdout.decode().split('\n'):
            log.info(line)

    if process.stderr:
        log.error(f'Run `{command}`, stderr.')
        for line in process.stderr.decode().split('\n'):
            log.error(line)

    return process.returncode

# docker-compose up
def compose_up(log:Logger, environment:dict, name:str) -> int:
    log.debug(f'Starting docker-compose.')
    for line in json.dumps(environment, indent = 4, sort_keys = True).split('\n'):
            log.debug(line)
    
    with open('./solution/.env', 'w') as file:
        for (key, value) in environment.items():
            print(f'{key}={value}', file = file)

    # Return exitcode.      
    return run_in_solution_dir(log, ['docker-compose', '-p', name, 'up', '-d'])

# docker-compose logs
# docker-compose down
def compose_down(log:Logger, name:str) -> int:
    assert run_in_solution_dir(log, ['docker-compose', '-p', name, 'logs']) == 0, \
        'Docker-compose logs returns non-zero status.'
    
    # Return exitcode.
    return run_in_solution_dir(log, ['docker-compose', '-p', name, 'down'])