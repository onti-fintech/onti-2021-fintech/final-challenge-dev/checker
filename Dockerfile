FROM python:3.9-alpine

# Update.
RUN apk update && apk upgrade

# GCC.
RUN apk add --no-cache --virtual .build-deps gcc musl-dev
# Docker compose.
RUN apk add --no-cache docker-compose

WORKDIR "/home/n2n-oracle"

# Environment.
COPY "requirements.txt" "requirements.txt"
RUN python -m pip install --upgrade pip
RUN python -m pip install -r "requirements.txt"

# Remove gcc.
RUN apk del .build-deps

# Copy files.
COPY . .
